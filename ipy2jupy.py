###
# title: sent ipython to jupyter
#
# language: python 3.5
# date: 2016-08-26
# author: bue
# license: GPLv3
#
# description:
#   parses a ipy2jupy formatted ipyton3 file and translates them into a jupyther notebook,
#   similar to pdflatex which translates tex into pdf.
#   feel free to use ipython magic commands like %timeit inline.
#   love to the non text editor folk.
#   peace to emacs and vim freaks.
#
# ipy2jupy she-magic commands:
#   a notebook have to have at least one chapter!
#
#   #%%%<chapter_name> : this is ipy2jupy magic.
#       this command will start a new notebook chapter.
#       the chapter name will be turned into the file name.
#       this command has to be at the very beginning of the line.
#
#   #%%<ipython_magic> : this are regular ipython magic commands used in
#       jupyther notebooks, but not used in command line ipython.
#       this command has to be at the very beginning of the line.
#
#   #%<command> : this are python commands used in
#       jupyther notebooks, but not used in command line ipython.
#       this command has to be at the very beginning of the line.
#
#   %<ipython_magic> : this are regular ipython magic commands used in
#       command line ipython and jupyther notebooks.
#       this command has to be at the very beginning of the line.
#
#   # : works inside a code and markdown block as usual.
#
#   ''' : works inside a code and markdown block as usual.
#
#   """ : this is ipy2jupy magic.
#       marks the begin and end of a markdown block.
#       this command has not to be at the very beginning of the line,
#       but everything else on the same line, like text or such, will be ignored.
#
# run:
#     put ipy2jupy.py and the file.ipy you like to translate into a notebook
#     into an empty folder.
#     python3 ipy2jupy.py file.ipy
#     jupyter notebook
####

import copy
import json
import re
import sys

# empty python3 notebook and cell dictionaries
d_notebook = {
    'cells': [],
 'nbformat_minor': 0,
 'metadata': {
     'kernelspec': {
         'display_name': 'Python 3',
         'language': 'python',
         'name': 'python3'}},
     #'language_info': {
     #    'codemirror_mode': {
     #       'name': 'ipython',
     #       'version': 3},
     #    'file_extension': '.py',
     #    'mimetype': 'text/x-python',
     #    'name': 'python',
     #    'nbconvert_exporter': 'python',
     #    'pygments_lexer': 'ipython3',
     #    'version': '3.4.5'},
 'nbformat': 4}

d_cellmarkdown = {
    'cell_type': 'markdown',
    'source': [],
    'metadata': {}}

d_cellcode = {
    'cell_type': 'code',
    'source': [],  # '# python code\n', 'print("hello world!")'
    'metadata': {'collapsed': True},
    'execution_count': None,
    'outputs': []}

# internal function
def strip2alphanum(s_in):
    """strip any non alphanumeric character out"""
    s_out = re.sub(r'[^a-zA-Z0-9]', '_', str(s_in))
    return(s_out)

# main function
if __name__ == "__main__":
    ### parser code ###
    print("translate ipython file into jupyter notebook ...")

    # read args to get input filename
    s_ifile = sys.argv[1]

    # open input file handle
    d_chapter = None
    s_ofile = None
    i_count = 0
    with open(s_ifile,'r') as f_in:
        s_celltype = None
        b_out = False
        for s_line in f_in:

            # increase counter and reset b_out
            i_count += 1
            b_out = True

            # chapter magic
            if (re.search(r'^#%%%', s_line)):
                if (d_chapter is not None):
                    if (s_celltype == "code"):
                        # append last code block to chapter
                        d_chapter["cells"].append(d_typesetter)
                        print("{} yay: end code block".format(i_count))
                    # write chapter to file
                    with open(s_ofile, 'w') as f_out:
                        f_out.write(json.dumps(d_chapter))
                        print("{} yay: end chapter".format(i_count))
                # start new chapter
                s_chapter = s_line.replace("#%%%","")
                s_chapter = s_chapter.strip()
                s_ofile = strip2alphanum(s_chapter)
                s_ofile =  s_ofile+".ipynb"
                d_chapter = copy.deepcopy(d_notebook)
                # make markdown title block
                d_typesetter =  copy.deepcopy(d_cellmarkdown)
                d_typesetter["source"].append("# "+s_chapter)
                d_chapter["cells"].append(d_typesetter)
                print("\n{} yay: new chapter".format(i_count))
                # reset cell type
                s_celltype = None

            # block magic
            else:
                if (d_chapter is not None):
                    # markdown translator
                    if (re.search('"""', s_line)):
                        # bue20160826: ''' will not be detected,
                        # so it can be part of a code or markdown block.
                        b_out = False
                        if (s_celltype == "markdown"):
                            # end of markdown
                            # append last block to chapter
                            s_celltype = None
                            d_chapter["cells"].append(d_typesetter)
                            print("{} yay: end markdown block".format(i_count))
                        elif (s_celltype == "code"):
                            # end of code block
                            # append last block to chapter
                            s_celltype = None
                            d_chapter["cells"].append(d_typesetter)
                            print("{} yay: end code block".format(i_count))
                            # new markdown block
                            s_celltype = "markdown"
                            d_typesetter = copy.deepcopy(d_cellmarkdown)
                            b_out = False
                            print("\n{} yay: begin markdown block".format(i_count))
                        else :
                            # new markdown block
                            s_celltype = "markdown"
                            d_typesetter = copy.deepcopy(d_cellmarkdown)
                            print("\n{} yay: begin markdown block".format(i_count))
                    # existing markdown block
                    elif (s_celltype == "markdown"):
                        pass

                    # code translator
                    elif (s_celltype == "code"):
                        # existing code block
                        pass
                    elif (len(s_line.strip()) > 0) and (s_celltype is None):
                        # new code block
                        s_celltype = "code"
                        d_typesetter =  copy.deepcopy(d_cellcode)
                        print("\n{} yay: begin code block".format(i_count))

                    # empty line between blocks
                    elif (len(s_line.strip()) <= 0) and (s_celltype is None):
                        b_out = False
                        print("\n{} yay: empty line between blocks".format(i_count))

                    # error
                    else:
                        sys.exit("Error ipython2jupyter: parse error.\nline: {}\nbacktack: {}".format(i_count, s_line))

                    # write line to block
                    if (b_out):
                        # ipy2jupy magic code command manipulation
                        if (s_celltype == "code"):
                            if (re.search(r'^#%%', s_line)) or (re.search(r'^#%', s_line)):
                                # ipython magic commands used in jupyter notebook only
                                # or commands used in jupyter notebook only
                                s_line = s_line.replace("#%","")
                            else:
                                # regular python or ipython magic command
                                pass
                        # append
                        d_typesetter["source"].append(s_line)
                        s_display = s_line.replace("\n","")
                        print("{} yay: block content: {}".format(i_count,s_display))

        # handle last chapter
        if (d_chapter is not None):
            # append block to chapter
            if (s_celltype == "code"):
                d_chapter["cells"].append(d_typesetter)
                print("{} yay: end code block".format(i_count))
            # write last chapter to file
            with open(s_ofile, 'w') as f_out:
                f_out.write(json.dumps(d_chapter))
                print("{} yay: end chapter".format(i_count))

    # end
    print("that's all folks, may the force be with you!")
